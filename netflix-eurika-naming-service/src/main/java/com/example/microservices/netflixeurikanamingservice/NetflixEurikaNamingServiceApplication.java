package com.example.microservices.netflixeurikanamingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class NetflixEurikaNamingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NetflixEurikaNamingServiceApplication.class, args);
	}
}
